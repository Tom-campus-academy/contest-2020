def triangleShapeStage1():
    z = -7
    x = 1
    n = 4
    for i in range(n):
        for j in range(z, n - i):
            print(' ', end='')
        for k in range(2 * i + x):
            print('*', end='')
        print()



def triangleShapeStage2():
    z = -7
    x = 3
    n = 4
    for i in range(4):
        for j in range(z, n - (i + 1)):
            if j == 1 and i == 0:
                print('0', end='')
            else :
                print(' ', end='')
        for k in range(3 * i + x):
            print('*', end='')
            if k == 2 and i == 0:
                print(' ' * 1 + '0', end='')
        print()
        z = z + 1
        x = x + 1



def triangleShapeStage3():
    z = -7
    x = 5
    n = 4
    for i in range(n):
        for j in range(z, n - (i + 2)):
            if i == 0 and j == - 3:
                print('0', end='')
            else:
                print(' ', end='')
        for k in range(4 * i + x):
            print('*', end='')
        if i == 0 and j == 1:
            print('    0', end='')
        print()
        z = z + 2
        x = x + 2

def pole_and_wire():
    for i in range(1):
        for j in range(4):
            print(' ', end='|')
        print(' *****', end=' |' * 4)
        print()

def pole_and_ball():
    for i in range(1):
        for j in range(4):
            print(' ', end='0')
        print(' *****', end=' 0' * 4)
        print()

def pole_end():
    for i in range(1):
        for j in range(9):
            print(' ', end='')
        print('*****')

def build_tree():

    triangleShapeStage1()
    triangleShapeStage2()
    triangleShapeStage3()
    pole_and_wire()
    pole_and_ball()
    pole_end()

build_tree()