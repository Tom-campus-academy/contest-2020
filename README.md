# **CONTEST CAMPUS ACADEMY 2020**

# Informations générales

Ici vous trouverez un programme codé en python afin de pouvoir créer un sapin de Noël constitué d'une étoile en son sommet, de guirlandes et de boules de Noël.  
Ce projet à été réalisé en seulement 1 jour et demi par des débutants en informatiques.

## Auteurs

**Florian HAMON** et **Tom COLLIN** deux étudiants en première année à **Campus Academy Rennes** ont participé à l'élaboration de ce projet. Ce projet a été réalisé en 24H.

## Comment faire ?

Une fois le dépot copié ouvrez votre invite de commande en tapant "cmd" dans votre barre de recherche windows en bas à gauche de votre écran.  

Dans votre **invite de commande** tapez :  

**python test.py**

## Développement en python

Ce projet à été développé en **python**. Nous nous sommes formés à ce language "sur le tas" en recherchant sur internet les conventions de ce laguage.