def double_tree():
    stage = 3  # Nombre d'étage fixe
    for l in range(stage):
        x = 2 * l + 1
        z = -7 # impose trois étages
        if l != 0:
            b = 1
        else:
            b = 0
        for i in range(4): # 4 branches par étage
            r = 0
            c = 0

# les espaces du premier sapin

            for j in range(z, 4 - (i + l)):
                if l != 0 and i == 0 and j == 1 - (l - 1) * 4:
                    print('0', end='')
                else:
                    print(' ', end='')
                    r = r+1

# les étoiles du premier sapin

            for k in range((2 + l) * i + x + b):
                if l != 0 and i == 0 and k == (2 + l) * i + x:
                    a = int((save - x) / 2 - 1)
                    print(' ' * a + '0', end='')

                elif k != (2 + l) * i + x + b - 1 and l != 0:
                    print('*', end='')
                elif l == 0:
                    print('*', end='')

#Espace inter sapin
            if i != 0 or l == 0:
                while(c < r):
                    print(' ',end='')
                    c += 1
                c = c + 1
            elif l==1 and i==0 :
                while (c < r-1):
                    print(' ', end='')
                    c += 1
            elif l == 2 and i == 0:
                while (c < r -4):
                    print(' ', end='')
                    c += 1
                c = c + 1

#Espace supplémentaire pour dernière branche
            print(' ', end='')

#Espace du second Sapin

            for v in range(11-l-l*i-i):
                if l != 0 and i == 0 and v == save + 1:
                    print('0',end='')
                elif l == 2 and i == 0 and v == 4:
                    print('0',end='')
                else:
                    print(' ', end='')

#Étoiles du second sapin

            for w in range(2*(l + 1) * i + (2 * l + 1)):
                if l == 1 and i==0 and w==2:
                    print('*'+' '*(a) +'0',end='')
                elif l == 2 and i == 0 and w == 4:
                    print('*'+' '*(a) +'0',end='')
               # else : print('*',end='')
                elif w != (2 + l) * i + x+b-1 and l!=0 :
                    print('*', end='')
                elif l==0 and w<2*(l+1)*i+(2*l+1)  :
                    print('*', end='')

#Retour à la ligne

            print()
            z = z + l
            x = x + l
            if i == 3:
                save = (2 + l) * 3 + x

def death_star():
    for i in range(7):
        for j in range(-6, 35):
            if (j == 0 or j == 5 or j == 10 or j == 24 or j == 29 or j == 34) and (i == 0):
                print('*', end='')
            elif (j == 2 or j == 5 or j == 8 or j == 26 or j == 29 or j == 32) and (i == 1):
                print('*', end='')
            elif (j == 5 or j == 29) and (i == 2 or i == 4):
                print('*', end='')
            elif (j == 0 or j == 2 or j == 4 or j == 6 or j == 8 or j == 10) and (i == 3):
                print('*', end='')
            elif (j == 24 or j == 26 or j == 28 or j == 30 or j == 32 or j == 34) and (i == 3):
                print('*', end='')
            elif (j == 2 or j == 8 or j == 26 or j == 32) and (i == 5):
                print('*', end='')
            elif (j == 5 or j == 29) and (i == 5):
                print('|', end='')
            elif (j == 0 or j == 10 or j == 24 or j == 34) and (i == 6):
                print('*', end='')
            elif (j == 5 or j == 29) and (i == 6):
                print('|', end='')
            else:
                print(' ', end='')
        print()

def pole_and_wire():
    a = 1
    for i in range(4):
        for j in range(4):
            print(' ', end='|')
        if a%2 == 0:
            print('  ', end='')
            a = a + 1
        else:
            print(' *****', end='')
            a = a + 1
    print()

def pole_and_ball():
    a=1
    for i in range(4):
        for j in range(4):
            print(' ', end='0')
        if a%2 == 0:
            print('  ', end='')
            a = a+1
        else :
            print(' *****', end='')
            a = a+1
    print()

def pole_end():
    for i in range(2):
        for j in range(9):
            print(' ', end='')
        print('*****',end=' '*10)


def build_tree():
    death_star()
    double_tree()
    pole_and_wire()
    pole_and_ball()
    pole_end()
    print()
    print(' '*18 +'Joyeux Noël !')

build_tree()